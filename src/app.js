import { addDonor,
        searchDonorByCnp,
        updateDonorFile,
        getDonorsByIntervalAndBloodType,
        checkInputCnp,
        clearInputCnp,
      } from "./app-script/donor-file-list";
import { buildDonorPage, buildUpdateDonorPage, buildLastDonors } from "./app-script/build-donor-page";
import { buildHistoryDonation } from "./app-script/build-donation";
import { addDonation } from "./app-script/donation-file-list";
import { buildDonorsPerBloodType } from "./app-script/build-donors-per-blood-type";
import { focusSearch, blurSearch } from "./app-script/helpers";
import { getBloodSupply } from "./app-script/blood-supply";

getBloodSupply();
const search = document.querySelector( ".search" );
const pickDate = document.querySelector( ".search-date" );
const inputSearch = document.querySelector( ".input-srch" );

search.addEventListener( "click", searchDonorByCnp, false );
inputSearch.addEventListener( "focus", focusSearch, false );
inputSearch.addEventListener( "blur", blurSearch, false );

if ( window.location.pathname === "/app-html/new-donor-page.html" ) {
    const saveFile = document.querySelector( ".addNewDonor" );
    const checkCnpBtn = document.getElementById( "input-cnp" );

    saveFile.addEventListener( "click", addDonor, false );
    checkCnpBtn.addEventListener( "blur", checkInputCnp, true );
    checkCnpBtn.addEventListener( "focus", clearInputCnp, true );
}
if ( window.location.pathname === "/index.html" ) {
    buildLastDonors();
    pickDate.addEventListener( "click", getDonorsByIntervalAndBloodType, false );
    inputSearch.addEventListener( "focus", focusSearch, false );
    inputSearch.addEventListener( "blur", blurSearch, false );
}

if ( window.location.pathname === "/app-html/donor-profile.html" ) {
    buildDonorPage();
    buildHistoryDonation();
}

if ( window.location.pathname === "/app-html/donor-update-page.html" ) {
    const updateDonor = document.querySelector( ".update-donor" );

    buildUpdateDonorPage();
    updateDonor.addEventListener( "click", updateDonorFile, false );
}

if ( window.location.pathname === "/app-html/add-new-donation-page.html" ) {
    const addDonationBtn = document.querySelector( ".add-donation" );

    addDonationBtn.addEventListener( "click", addDonation, false );
}

if ( window.location.pathname === "/app-html/donors-per-bloodType.html" ) {
    buildDonorsPerBloodType();
}
