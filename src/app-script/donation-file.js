import generateRandomID from "./utils";

const DonationFile = function DonorFile( data ) {
    let donorID = JSON.parse( localStorage.getItem( "DonorFile" ) );
    donorID = donorID[ 0 ];
    const donationID = generateRandomID();

    for ( let i = 0; i < data.length; i += 1 ) {
        const date = data[ 0 ].value;
        const quantity = data[ 1 ].value;
        const gloves = data[ 2 ].value;
        const bandages = data[ 3 ].value;
        const syringes = data[ 4 ].value;
        const disinfectant = data[ 5 ].value;

        const donationInfo = {
            date,
            quantity,
        };
        const donationMaterials = {
            gloves,
            bandages,
            syringes,
            disinfectant,
        };

        return [
            donorID, donationID, donationInfo, donationMaterials,
        ];
    }
};

export default DonationFile;
