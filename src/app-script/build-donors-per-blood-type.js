import mustache from "mustache";

const buildDonorsPerBloodType = () => {
    const view = JSON.parse( localStorage.getItem( "Lista" ) );
    let donorsMarkup = "";
    const periodTable = document.getElementById( "donors-blood-type-table" );

    for ( let i = 0; i < view.length; i += 1 ) {
        const donorsName = view[ i ].donorsName;
        const donationsQuantity = view[ i ].donationsQuantity;
        const donationsDate = view[ i ].donationsDate;
        const getFromDay = new Date( view[ i ].periodStart ).getDate();
        const getToDay = new Date( view[ i ].periodEnd ).getDate();
        const getFromMonth = new Date( view[ i ].periodStart ).getMonth();
        const getToMonth = new Date( view[ i ].periodEnd ).getMonth();
        const getFromYear = new Date( view[ i ].periodStart ).getFullYear();
        const getToYear = new Date( view[ i ].periodEnd ).getFullYear();
        const templateForDate = `From <span>${ getFromDay }/${ getFromMonth + 1 }/${ getFromYear }</span>` +
        `  To <span>${ getToDay }/${ getToMonth + 1 }/${ getToYear }</span>`;
        const templateForTitle = "<span>{{bloodType}}</span>, BLOOD TYPE DONORS";
        donorsMarkup += "<tr>" +
                          `<td>${ i + 1 }</td>` +
                          `<td>${ donorsName }</td>` +
                          `<td>${ donationsQuantity }</td>` +
                          `<td>${ donationsDate }</td>` +
                        "</tr>";
        const bloodType = mustache.render( templateForTitle, view[ i ] );

        document.getElementById( "pickDate" ).innerHTML = templateForDate;
        document.querySelector( "h3" ).innerHTML = bloodType;
    }

    periodTable.innerHTML += donorsMarkup;
};

export { buildDonorsPerBloodType };
