import DonorFile from "./donor-file";

const checkInputValues = ( data ) => {
    let flag = true;

    for ( let i = 0; i < data.length; i += 1 ) {
        const currenInput = data[ i ];
        if ( currenInput.value === "" ) {
            currenInput.style.color = "#aec2e2";
            currenInput.style.border = "1px solid red";
            currenInput.placeholder = "Please complete this field!!";

            currenInput.onclick = function onclick() {
                this.style.background = "white";
                this.style.color = "black";
                this.style.border = "none";
                this.style.borderBottom = "1px solid #707cd2";
            };
            flag = false;
        }
    }

    return flag;
};

const addDonors = ( donor ) => {
    let donors = JSON.parse( localStorage.getItem( "DonorsList" ) );

    if ( !donors ) {
        donors = [ ];
    }

    donors.push( donor );
    localStorage.setItem( "DonorsList", JSON.stringify( donors ) );

    return donors;
};

const addDonor = () => {
    const form = document.getElementById( "donor-form" );
    const check = checkInputValues( form );

    if ( check === true ) {
        const newDonor = new DonorFile( form );

        localStorage.setItem( "DonorFile", JSON.stringify( newDonor ) );
        addDonors( newDonor );
        window.location.href = "donor-profile.html";
    }
};

const getDonorFile = () => {
    const donorFile = JSON.parse( localStorage.getItem( "DonorFile" ) );

    return donorFile;
};

const getDonorByCnp = ( cnp ) => {
    const donors = JSON.parse( localStorage.getItem( "DonorsList" ) );

    for ( let i = 0; i < donors.length; i += 1 ) {
        if ( donors[ i ][ 1 ].cnp === cnp ) {
            localStorage.setItem( "DonorFile", JSON.stringify( donors[ i ] ) );

            return donors[ i ];
        }
    }

    return false;
};

const searchDonorByCnp = () => {
    const donorCnp = document.querySelector( ".input-srch" ).value.trim();
    const foundDonor = getDonorByCnp( donorCnp );

    if ( foundDonor !== false && foundDonor !== "" ) {
        if ( window.location.pathname === "/index.html" ) {
            window.location.href = "../app-html/donor-profile.html";
        } else {
            window.location.href = "donor-profile.html";
        }
    } else {
        alert( "No donor with this CNP exists!" );
    }
};

const getLastDonors = () => {
    const donorFile = JSON.parse( localStorage.getItem( "DonorsList" ) );

    return donorFile;
};

const getBloodType = () => {
    const select = document.querySelector( "select" );
    const optionValue = select.options[ select.selectedIndex ].value;

    return optionValue;
};

const getDateValues = () => {
    const fromDate = document.getElementById( "fromDate" );
    const toDate = document.getElementById( "toDate" );
    const timeStampFromDate = Date.parse( fromDate.value );
    const timeStampToDate = Date.parse( toDate.value );
    const bloodTypeValue = getBloodType();

    return {
        bloodTypeValue,
        timeStampFromDate,
        timeStampToDate,
    };
};

const getDonorsByIntervalAndBloodType = () => {
    const getValues = getDateValues();
    const periodStart = getValues.timeStampFromDate;
    const periodEnd = getValues.timeStampToDate;
    const bloodType = getValues.bloodTypeValue;
    const donations = JSON.parse( localStorage.getItem( "DonationsList" ) );
    const donors = JSON.parse( localStorage.getItem( "DonorsList" ) );
    let lista;
    let foundDonors;

    for ( let i = 0; i < donations.length; i += 1 ) {
        const donationsDate = donations[ i ][ 2 ].date;
        const dateTimeStamp = Date.parse( donationsDate );
        const donationsQuantity = donations[ i ][ 2 ].quantity;
        const donationsDonorID = donations[ i ][ 0 ].donorID;

        for ( let j = 0; j < donors.length; j += 1 ) {
            const donorsBloodType = donors[ j ][ 3 ].bloodType;
            const donorsName = donors[ j ][ 1 ].name;
            const donorID = donors[ j ][ 0 ].donorID;
            const from = periodStart <= dateTimeStamp;
            const to = periodEnd >= dateTimeStamp;
            if ( from && to && donorsBloodType === bloodType && donationsDonorID === donorID ) {
                if ( !lista ) {
                    lista = [ ];
                }
                foundDonors = {
                    donorsName,
                    periodStart,
                    periodEnd,
                    donationsQuantity,
                    donationsDate,
                    bloodType,
                };
                lista.push( foundDonors );
            }
        }
    }
    localStorage.setItem( "Lista", JSON.stringify( lista ) );
    if ( lista === undefined ) {
        const error = document.getElementById( "error" );

        error.innerHTML = "No donors match !";
        error.style.color = "red";
        error.style.fontSize = "28px";
    } else {
        window.location.href = "/app-html/donors-per-bloodType.html";
    }
    return lista;
};

const updateDonorFile = () => {
    const updateDonorForm = document.getElementById( "donor-form" );
    const oldDonor = JSON.parse( localStorage.getItem( "DonorFile" ) );
    const newDonor = new DonorFile( updateDonorForm );
    const donors = JSON.parse( localStorage.getItem( "DonorsList" ) );
    newDonor[ 0 ].donorID = oldDonor[ 0 ].donorID;

    for ( let i = 0; i < donors.length; i += 1 ) {
        if ( donors[ i ][ 0 ].donorID === oldDonor[ 0 ].donorID ) {
            donors.splice( i, 1 );
        }
    }

    donors.push( newDonor );
    localStorage.setItem( "DonorFile", JSON.stringify( newDonor ) );
    localStorage.setItem( "DonorsList", JSON.stringify( donors ) );
    window.location.href = "donor-profile.html";
};

const checkInputCnp = () => {
    const formValues = document.getElementById( "donor-form" );
    const cnp = formValues[ 0 ].value;
    const donorsCnp = getDonorByCnp( cnp );

    if ( donorsCnp[ 1 ].cnp === cnp ) {
        document.getElementById( "errors" ).innerHTML = "Please enter a valid CNP!!!";
    }
};

const clearInputCnp = () => {
    document.getElementById( "errors" ).innerHTML = "";

    const formValues = document.getElementById( "donor-form" );
    formValues[ 0 ].value = "";
};

export { addDonor,
        getDonorFile,
        searchDonorByCnp,
        getDonorByCnp,
        updateDonorFile,
        getLastDonors,
        getDateValues,
        getDonorsByIntervalAndBloodType,
        checkInputCnp,
        clearInputCnp,
        blur,
      };
