import mustache from "mustache";
import { getDonorFile, getLastDonors } from "./donor-file-list";

const buildDonorPage = () => {
    const view = getDonorFile();
    const templateForDonorID = "File number: <span>{{donorID}}</span>";
    const templateForPersonalInfo = "<div>CNP: <span class='form'>{{cnp}}</span></div>" +
                                    "<div>Full Name <span class='form'>{{name}}</span></div>" +
                                    "<div>Date of Birth <span class='form'> {{dateOfBirth}}</span></div>" +
                                    "<div>Person Sex <span class='form'>{{sex}}</span></div>" +
                                    "<div>Address <span class='form'>{{address}}</span></div>" +
                                    "<div>Phone Number <span class='form'>{{phone}}</span></div>" +
                                    "<div>Email <span class='form'>{{email}}</span></div>";
    const templateForMedicalHistory = "<div>Health Condition <span class='form'>{{healthCondition}}</span></div>" +
                                      "<div>History of Diseases <span class='form'>{{historyDiseases}}</span></div>";
    const templateForBoodDetails = "<div>Blood Type: <span class='form'>{{bloodType}}</span></div>" +
                                   "<div>Blood Rh: <span class='form'>{{rh}}</span></div>" +
                                   "<div>Other Details: <span class='form'>{{otherDetails}}</span></div>";

    const personalInfo = mustache.render( templateForPersonalInfo, view[ 1 ] );
    const medicalHistory = mustache.render( templateForMedicalHistory, view[ 2 ] );
    const bloodDetails = mustache.render( templateForBoodDetails, view[ 3 ] );
    const donorID = mustache.render( templateForDonorID, view[ 0 ] );

    document.getElementById( "donorID" ).innerHTML = donorID;
    document.getElementById( "personalInfo" ).innerHTML = personalInfo;
    document.getElementById( "medicalHistory" ).innerHTML = medicalHistory;
    document.getElementById( "bloodDetails" ).innerHTML = bloodDetails;
    return [
        personalInfo, medicalHistory, bloodDetails, donorID,
    ];
};

const buildUpdateDonorPage = ( ) => {
    const view = getDonorFile();
    const templateForDonorID = "File number: <span>{{donorID}}</span>";
    const templateForPersonalInfo =
    "<div>CNP" +
      "<input class='form' id='input-cnp' maxlength='11' value='{{cnp}}'>" +
    "</div>" +
    "<div>Full Name" +
      "<input class='form' type='text' id='input-name' value='{{name}}'>" +
    "</div>" +
    "<div>Date of Birth" +
      "<input class='form' type='date' value='{{dateOfBirth}}'>" +
    "</div>" +
    "<div>Person Sex" +
      "<input class='form' type='text' value='{{sex}}'>" +
    "</div>" +
    "<div>address" +
      "<input class='form' type='text' value='{{address}}'>" +
    "</div>" +
    "<div>Phone Number" +
      "<input class='form' type='tel' value='{{phone}}'>" +
    "</div>" +
    "<div>Email" +
      "<input class='form' type='email' value='{{email}}'>" +
    "</div>";
    const templateForMedicalHistory =
    "<textarea class='contact form' rows='7'>{{healthCondition}}</textarea>" +
    "<textarea class='contact form' rows='7'>{{historyDiseases}}</textarea>";
    const templateForBoodDetails =
    "<div>Blood Type" +
      "<input class='form' type='text' value='{{bloodType}}'>" +
    "</div>" +
    "<div>Rh" +
      "<input class='form' type='text' value='{{rh}}'>" +
    "</div>" +
    "<textarea class='contact form' rows='7'>{{otherDetails}}</textarea>";

    const donorID = mustache.render( templateForDonorID, view[ 0 ] );
    const personalInfo = mustache.render( templateForPersonalInfo, view[ 1 ] );
    const medicalHistory = mustache.render( templateForMedicalHistory, view[ 2 ] );
    const bloodDetails = mustache.render( templateForBoodDetails, view[ 3 ] );

    document.getElementById( "donorID" ).innerHTML = donorID;
    document.getElementById( "personalInfo" ).innerHTML = personalInfo;
    document.getElementById( "medicalHistory" ).innerHTML = medicalHistory;
    document.getElementById( "bloodDetails" ).innerHTML = bloodDetails;
    return [
        personalInfo, medicalHistory, bloodDetails, donorID,
    ];
};

const buildLastDonors = () => {
    const view = getLastDonors();
    let donorsMarkup = "";
    const historyTable = document.getElementById( "last-donors-table" );

    for ( let i = 0; i < view.length; i += 1 ) {
        if ( i < 10 ) {
            const bloodType = view[ i ][ 3 ].bloodType;
            const rh = view[ i ][ 3 ].rh;
            const name = view[ i ][ 1 ].name;

            donorsMarkup += "<tr>" +
                              `<td>${ i + 1 }</td>` +
                              `<td>${ name }</td>` +
                              `<td>${ bloodType }</td>` +
                              `<td>${ rh }</td>` +
                            "</tr>";
        }
    }
    historyTable.innerHTML += donorsMarkup;
};
export { buildDonorPage, buildUpdateDonorPage, buildLastDonors };
