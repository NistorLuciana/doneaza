import generateRandomId from "./utils";

const DonorFile = function DonorFile( data ) {
    const donorID = generateRandomId();
    for ( let i = 0; i < data.length; i += 1 ) {
        const cnp = data[ 0 ].value;
        const name = data[ 1 ].value;
        const dateOfBirth = data[ 2 ].value;
        const sex = data[ 3 ].value;
        const address = data[ 4 ].value;
        const phone = data[ 5 ].value;
        const email = data[ 6 ].value;
        const healthCondition = data[ 7 ].value;
        const historyDiseases = data[ 8 ].value;
        const bloodType = data[ 9 ].value;
        const rh = data[ 10 ].value;
        const otherDetails = data[ 11 ].value;

        const personalInfo = {
            cnp,
            name,
            dateOfBirth,
            sex,
            address,
            phone,
            email,
        };
        const medicalHistory = {
            healthCondition,
            historyDiseases,
        };
        const bloodDetails = {
            bloodType,
            rh,
            otherDetails,
        };

        const donorId = {
            donorID,
        };

        return [
            donorId, personalInfo, medicalHistory, bloodDetails,
        ];
    }
};
export default DonorFile;
