import DonationFile from "./donation-file";
import { getDonorFile } from "./donor-file-list";

const addDonations = ( donation ) => {
    let donations = JSON.parse( localStorage.getItem( "DonationsList" ) );

    if ( !donations ) {
        donations = [ ];
    }

    donations.push( donation );
    localStorage.setItem( "DonationsList", JSON.stringify( donations ) );

    return donations;
};

const addDonation = () => {
    const formValues = document.querySelectorAll( ".form" );
    const newDonation = new DonationFile( formValues );

    localStorage.setItem( "DonationFile", JSON.stringify( newDonation ) );
    addDonations( newDonation );
};

const getHistoryDonations = () => {
    const donorFile = getDonorFile();
    const donationsHistory = JSON.parse( localStorage.getItem( "DonationsList" ) );
    const arr = donationsHistory.filter( function foundDonors( donor ) {
        if ( donor[ 0 ].donorID === donorFile[ 0 ].donorID ) {
            return donor;
        }

        return arr;
    } );

    return arr;
};

export { addDonation, getHistoryDonations };
