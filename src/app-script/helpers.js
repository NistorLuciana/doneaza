const search = document.querySelector( ".search" );
const inputSearch = document.querySelector( ".input-srch" );

const focusSearch = () => {
    inputSearch.style.width = "340px";
    inputSearch.style.transition = "width 2s";
    search.style.left = "610px";
    search.style.transition = "left 2s";
};

const blurSearch = () => {
    inputSearch.style.width = "180px";
    search.style.left = "450px";
};

export { focusSearch, blurSearch };
