import { getHistoryDonations } from "./donation-file-list";

const buildHistoryDonation = () => {
    const view = getHistoryDonations();
    let donorsMarkup = "";
    const historyTable = document.getElementById( "history-donation" );

    for ( let i = 0; i < view.length; i += 1 ) {
        const date = view[ i ][ 2 ].date;
        const quantity = view[ i ][ 2 ].quantity;

        donorsMarkup += "<tr>" +
                          `<td>${ i + 1 }</td>` +
                          `<td>${ date }</td>` +
                          `<td>${ quantity }</td>` +
                        "</tr>";
    }
    historyTable.innerHTML += donorsMarkup;
};

export { buildHistoryDonation };
