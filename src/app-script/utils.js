const generateRandomId = () => {
    const id = String( Date.now() ) + Math.floor( Math.random() * 100 );

    return id;
};

export default generateRandomId;
