/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.blur = exports.clearInputCnp = exports.checkInputCnp = exports.getDonorsByIntervalAndBloodType = exports.getDateValues = exports.getLastDonors = exports.updateDonorFile = exports.getDonorByCnp = exports.searchDonorByCnp = exports.getDonorFile = exports.addDonor = undefined;

var _donorFile = __webpack_require__(8);

var _donorFile2 = _interopRequireDefault(_donorFile);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var checkInputValues = function checkInputValues(data) {
    var flag = true;

    for (var i = 0; i < data.length; i += 1) {
        var currenInput = data[i];
        if (currenInput.value === "") {
            currenInput.style.color = "#aec2e2";
            currenInput.style.border = "1px solid red";
            currenInput.placeholder = "Please complete this field!!";

            currenInput.onclick = function onclick() {
                this.style.background = "white";
                this.style.color = "black";
                this.style.border = "none";
                this.style.borderBottom = "1px solid #707cd2";
            };
            flag = false;
        }
    }

    return flag;
};

var addDonors = function addDonors(donor) {
    var donors = JSON.parse(localStorage.getItem("DonorsList"));

    if (!donors) {
        donors = [];
    }

    donors.push(donor);
    localStorage.setItem("DonorsList", JSON.stringify(donors));

    return donors;
};

var addDonor = function addDonor() {
    var form = document.getElementById("donor-form");
    var check = checkInputValues(form);

    if (check === true) {
        var newDonor = new _donorFile2.default(form);

        localStorage.setItem("DonorFile", JSON.stringify(newDonor));
        addDonors(newDonor);
        window.location.href = "donor-profile.html";
    }
};

var getDonorFile = function getDonorFile() {
    var donorFile = JSON.parse(localStorage.getItem("DonorFile"));

    return donorFile;
};

var getDonorByCnp = function getDonorByCnp(cnp) {
    var donors = JSON.parse(localStorage.getItem("DonorsList"));

    for (var i = 0; i < donors.length; i += 1) {
        if (donors[i][1].cnp === cnp) {
            localStorage.setItem("DonorFile", JSON.stringify(donors[i]));

            return donors[i];
        }
    }

    return false;
};

var searchDonorByCnp = function searchDonorByCnp() {
    var donorCnp = document.querySelector(".input-srch").value.trim();
    var foundDonor = getDonorByCnp(donorCnp);

    if (foundDonor !== false && foundDonor !== "") {
        if (window.location.pathname === "/index.html") {
            window.location.href = "../app-html/donor-profile.html";
        } else {
            window.location.href = "donor-profile.html";
        }
    } else {
        alert("No donor with this CNP exists!");
    }
};

var getLastDonors = function getLastDonors() {
    var donorFile = JSON.parse(localStorage.getItem("DonorsList"));

    return donorFile;
};

var getBloodType = function getBloodType() {
    var select = document.querySelector("select");
    var optionValue = select.options[select.selectedIndex].value;

    return optionValue;
};

var getDateValues = function getDateValues() {
    var fromDate = document.getElementById("fromDate");
    var toDate = document.getElementById("toDate");
    var timeStampFromDate = Date.parse(fromDate.value);
    var timeStampToDate = Date.parse(toDate.value);
    var bloodTypeValue = getBloodType();

    return {
        bloodTypeValue: bloodTypeValue,
        timeStampFromDate: timeStampFromDate,
        timeStampToDate: timeStampToDate
    };
};

var getDonorsByIntervalAndBloodType = function getDonorsByIntervalAndBloodType() {
    var getValues = getDateValues();
    var periodStart = getValues.timeStampFromDate;
    var periodEnd = getValues.timeStampToDate;
    var bloodType = getValues.bloodTypeValue;
    var donations = JSON.parse(localStorage.getItem("DonationsList"));
    var donors = JSON.parse(localStorage.getItem("DonorsList"));
    var lista = void 0;
    var foundDonors = void 0;

    for (var i = 0; i < donations.length; i += 1) {
        var donationsDate = donations[i][2].date;
        var dateTimeStamp = Date.parse(donationsDate);
        var donationsQuantity = donations[i][2].quantity;
        var donationsDonorID = donations[i][0].donorID;

        for (var j = 0; j < donors.length; j += 1) {
            var donorsBloodType = donors[j][3].bloodType;
            var donorsName = donors[j][1].name;
            var donorID = donors[j][0].donorID;
            var from = periodStart <= dateTimeStamp;
            var to = periodEnd >= dateTimeStamp;
            if (from && to && donorsBloodType === bloodType && donationsDonorID === donorID) {
                if (!lista) {
                    lista = [];
                }
                foundDonors = {
                    donorsName: donorsName,
                    periodStart: periodStart,
                    periodEnd: periodEnd,
                    donationsQuantity: donationsQuantity,
                    donationsDate: donationsDate,
                    bloodType: bloodType
                };
                lista.push(foundDonors);
            }
        }
    }
    localStorage.setItem("Lista", JSON.stringify(lista));
    if (lista === undefined) {
        var error = document.getElementById("error");

        error.innerHTML = "No donors match !";
        error.style.color = "red";
        error.style.fontSize = "28px";
    } else {
        window.location.href = "/app-html/donors-per-bloodType.html";
    }
    return lista;
};

var updateDonorFile = function updateDonorFile() {
    var updateDonorForm = document.getElementById("donor-form");
    var oldDonor = JSON.parse(localStorage.getItem("DonorFile"));
    var newDonor = new _donorFile2.default(updateDonorForm);
    var donors = JSON.parse(localStorage.getItem("DonorsList"));
    newDonor[0].donorID = oldDonor[0].donorID;

    for (var i = 0; i < donors.length; i += 1) {
        if (donors[i][0].donorID === oldDonor[0].donorID) {
            donors.splice(i, 1);
        }
    }

    donors.push(newDonor);
    localStorage.setItem("DonorFile", JSON.stringify(newDonor));
    localStorage.setItem("DonorsList", JSON.stringify(donors));
    window.location.href = "donor-profile.html";
};

var checkInputCnp = function checkInputCnp() {
    var formValues = document.getElementById("donor-form");
    var cnp = formValues[0].value;
    var donorsCnp = getDonorByCnp(cnp);

    if (donorsCnp[1].cnp === cnp) {
        document.getElementById("errors").innerHTML = "Please enter a valid CNP!!!";
    }
};

var clearInputCnp = function clearInputCnp() {
    document.getElementById("errors").innerHTML = "";

    var formValues = document.getElementById("donor-form");
    formValues[0].value = "";
};

exports.addDonor = addDonor;
exports.getDonorFile = getDonorFile;
exports.searchDonorByCnp = searchDonorByCnp;
exports.getDonorByCnp = getDonorByCnp;
exports.updateDonorFile = updateDonorFile;
exports.getLastDonors = getLastDonors;
exports.getDateValues = getDateValues;
exports.getDonorsByIntervalAndBloodType = getDonorsByIntervalAndBloodType;
exports.checkInputCnp = checkInputCnp;
exports.clearInputCnp = clearInputCnp;
exports.blur = blur;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getHistoryDonations = exports.addDonation = undefined;

var _donationFile = __webpack_require__(7);

var _donationFile2 = _interopRequireDefault(_donationFile);

var _donorFileList = __webpack_require__(0);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var addDonations = function addDonations(donation) {
    var donations = JSON.parse(localStorage.getItem("DonationsList"));

    if (!donations) {
        donations = [];
    }

    donations.push(donation);
    localStorage.setItem("DonationsList", JSON.stringify(donations));

    return donations;
};

var addDonation = function addDonation() {
    var formValues = document.querySelectorAll(".form");
    var newDonation = new _donationFile2.default(formValues);

    localStorage.setItem("DonationFile", JSON.stringify(newDonation));
    addDonations(newDonation);
};

var getHistoryDonations = function getHistoryDonations() {
    var donorFile = (0, _donorFileList.getDonorFile)();
    var donationsHistory = JSON.parse(localStorage.getItem("DonationsList"));
    var arr = donationsHistory.filter(function foundDonors(donor) {
        if (donor[0].donorID === donorFile[0].donorID) {
            return donor;
        }

        return arr;
    });

    return arr;
};

exports.addDonation = addDonation;
exports.getHistoryDonations = getHistoryDonations;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var generateRandomId = function generateRandomId() {
    var id = String(Date.now()) + Math.floor(Math.random() * 100);

    return id;
};

exports.default = generateRandomId;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * mustache.js - Logic-less {{mustache}} templates with JavaScript
 * http://github.com/janl/mustache.js
 */

/*global define: false Mustache: true*/

(function defineMustache (global, factory) {
  if (typeof exports === 'object' && exports && typeof exports.nodeName !== 'string') {
    factory(exports); // CommonJS
  } else if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)); // AMD
  } else {
    global.Mustache = {};
    factory(global.Mustache); // script, wsh, asp
  }
}(this, function mustacheFactory (mustache) {

  var objectToString = Object.prototype.toString;
  var isArray = Array.isArray || function isArrayPolyfill (object) {
    return objectToString.call(object) === '[object Array]';
  };

  function isFunction (object) {
    return typeof object === 'function';
  }

  /**
   * More correct typeof string handling array
   * which normally returns typeof 'object'
   */
  function typeStr (obj) {
    return isArray(obj) ? 'array' : typeof obj;
  }

  function escapeRegExp (string) {
    return string.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
  }

  /**
   * Null safe way of checking whether or not an object,
   * including its prototype, has a given property
   */
  function hasProperty (obj, propName) {
    return obj != null && typeof obj === 'object' && (propName in obj);
  }

  // Workaround for https://issues.apache.org/jira/browse/COUCHDB-577
  // See https://github.com/janl/mustache.js/issues/189
  var regExpTest = RegExp.prototype.test;
  function testRegExp (re, string) {
    return regExpTest.call(re, string);
  }

  var nonSpaceRe = /\S/;
  function isWhitespace (string) {
    return !testRegExp(nonSpaceRe, string);
  }

  var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  };

  function escapeHtml (string) {
    return String(string).replace(/[&<>"'`=\/]/g, function fromEntityMap (s) {
      return entityMap[s];
    });
  }

  var whiteRe = /\s*/;
  var spaceRe = /\s+/;
  var equalsRe = /\s*=/;
  var curlyRe = /\s*\}/;
  var tagRe = /#|\^|\/|>|\{|&|=|!/;

  /**
   * Breaks up the given `template` string into a tree of tokens. If the `tags`
   * argument is given here it must be an array with two string values: the
   * opening and closing tags used in the template (e.g. [ "<%", "%>" ]). Of
   * course, the default is to use mustaches (i.e. mustache.tags).
   *
   * A token is an array with at least 4 elements. The first element is the
   * mustache symbol that was used inside the tag, e.g. "#" or "&". If the tag
   * did not contain a symbol (i.e. {{myValue}}) this element is "name". For
   * all text that appears outside a symbol this element is "text".
   *
   * The second element of a token is its "value". For mustache tags this is
   * whatever else was inside the tag besides the opening symbol. For text tokens
   * this is the text itself.
   *
   * The third and fourth elements of the token are the start and end indices,
   * respectively, of the token in the original template.
   *
   * Tokens that are the root node of a subtree contain two more elements: 1) an
   * array of tokens in the subtree and 2) the index in the original template at
   * which the closing tag for that section begins.
   */
  function parseTemplate (template, tags) {
    if (!template)
      return [];

    var sections = [];     // Stack to hold section tokens
    var tokens = [];       // Buffer to hold the tokens
    var spaces = [];       // Indices of whitespace tokens on the current line
    var hasTag = false;    // Is there a {{tag}} on the current line?
    var nonSpace = false;  // Is there a non-space char on the current line?

    // Strips all whitespace tokens array for the current line
    // if there was a {{#tag}} on it and otherwise only space.
    function stripSpace () {
      if (hasTag && !nonSpace) {
        while (spaces.length)
          delete tokens[spaces.pop()];
      } else {
        spaces = [];
      }

      hasTag = false;
      nonSpace = false;
    }

    var openingTagRe, closingTagRe, closingCurlyRe;
    function compileTags (tagsToCompile) {
      if (typeof tagsToCompile === 'string')
        tagsToCompile = tagsToCompile.split(spaceRe, 2);

      if (!isArray(tagsToCompile) || tagsToCompile.length !== 2)
        throw new Error('Invalid tags: ' + tagsToCompile);

      openingTagRe = new RegExp(escapeRegExp(tagsToCompile[0]) + '\\s*');
      closingTagRe = new RegExp('\\s*' + escapeRegExp(tagsToCompile[1]));
      closingCurlyRe = new RegExp('\\s*' + escapeRegExp('}' + tagsToCompile[1]));
    }

    compileTags(tags || mustache.tags);

    var scanner = new Scanner(template);

    var start, type, value, chr, token, openSection;
    while (!scanner.eos()) {
      start = scanner.pos;

      // Match any text between tags.
      value = scanner.scanUntil(openingTagRe);

      if (value) {
        for (var i = 0, valueLength = value.length; i < valueLength; ++i) {
          chr = value.charAt(i);

          if (isWhitespace(chr)) {
            spaces.push(tokens.length);
          } else {
            nonSpace = true;
          }

          tokens.push([ 'text', chr, start, start + 1 ]);
          start += 1;

          // Check for whitespace on the current line.
          if (chr === '\n')
            stripSpace();
        }
      }

      // Match the opening tag.
      if (!scanner.scan(openingTagRe))
        break;

      hasTag = true;

      // Get the tag type.
      type = scanner.scan(tagRe) || 'name';
      scanner.scan(whiteRe);

      // Get the tag value.
      if (type === '=') {
        value = scanner.scanUntil(equalsRe);
        scanner.scan(equalsRe);
        scanner.scanUntil(closingTagRe);
      } else if (type === '{') {
        value = scanner.scanUntil(closingCurlyRe);
        scanner.scan(curlyRe);
        scanner.scanUntil(closingTagRe);
        type = '&';
      } else {
        value = scanner.scanUntil(closingTagRe);
      }

      // Match the closing tag.
      if (!scanner.scan(closingTagRe))
        throw new Error('Unclosed tag at ' + scanner.pos);

      token = [ type, value, start, scanner.pos ];
      tokens.push(token);

      if (type === '#' || type === '^') {
        sections.push(token);
      } else if (type === '/') {
        // Check section nesting.
        openSection = sections.pop();

        if (!openSection)
          throw new Error('Unopened section "' + value + '" at ' + start);

        if (openSection[1] !== value)
          throw new Error('Unclosed section "' + openSection[1] + '" at ' + start);
      } else if (type === 'name' || type === '{' || type === '&') {
        nonSpace = true;
      } else if (type === '=') {
        // Set the tags for the next time around.
        compileTags(value);
      }
    }

    // Make sure there are no open sections when we're done.
    openSection = sections.pop();

    if (openSection)
      throw new Error('Unclosed section "' + openSection[1] + '" at ' + scanner.pos);

    return nestTokens(squashTokens(tokens));
  }

  /**
   * Combines the values of consecutive text tokens in the given `tokens` array
   * to a single token.
   */
  function squashTokens (tokens) {
    var squashedTokens = [];

    var token, lastToken;
    for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) {
      token = tokens[i];

      if (token) {
        if (token[0] === 'text' && lastToken && lastToken[0] === 'text') {
          lastToken[1] += token[1];
          lastToken[3] = token[3];
        } else {
          squashedTokens.push(token);
          lastToken = token;
        }
      }
    }

    return squashedTokens;
  }

  /**
   * Forms the given array of `tokens` into a nested tree structure where
   * tokens that represent a section have two additional items: 1) an array of
   * all tokens that appear in that section and 2) the index in the original
   * template that represents the end of that section.
   */
  function nestTokens (tokens) {
    var nestedTokens = [];
    var collector = nestedTokens;
    var sections = [];

    var token, section;
    for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) {
      token = tokens[i];

      switch (token[0]) {
        case '#':
        case '^':
          collector.push(token);
          sections.push(token);
          collector = token[4] = [];
          break;
        case '/':
          section = sections.pop();
          section[5] = token[2];
          collector = sections.length > 0 ? sections[sections.length - 1][4] : nestedTokens;
          break;
        default:
          collector.push(token);
      }
    }

    return nestedTokens;
  }

  /**
   * A simple string scanner that is used by the template parser to find
   * tokens in template strings.
   */
  function Scanner (string) {
    this.string = string;
    this.tail = string;
    this.pos = 0;
  }

  /**
   * Returns `true` if the tail is empty (end of string).
   */
  Scanner.prototype.eos = function eos () {
    return this.tail === '';
  };

  /**
   * Tries to match the given regular expression at the current position.
   * Returns the matched text if it can match, the empty string otherwise.
   */
  Scanner.prototype.scan = function scan (re) {
    var match = this.tail.match(re);

    if (!match || match.index !== 0)
      return '';

    var string = match[0];

    this.tail = this.tail.substring(string.length);
    this.pos += string.length;

    return string;
  };

  /**
   * Skips all text until the given regular expression can be matched. Returns
   * the skipped string, which is the entire tail if no match can be made.
   */
  Scanner.prototype.scanUntil = function scanUntil (re) {
    var index = this.tail.search(re), match;

    switch (index) {
      case -1:
        match = this.tail;
        this.tail = '';
        break;
      case 0:
        match = '';
        break;
      default:
        match = this.tail.substring(0, index);
        this.tail = this.tail.substring(index);
    }

    this.pos += match.length;

    return match;
  };

  /**
   * Represents a rendering context by wrapping a view object and
   * maintaining a reference to the parent context.
   */
  function Context (view, parentContext) {
    this.view = view;
    this.cache = { '.': this.view };
    this.parent = parentContext;
  }

  /**
   * Creates a new context using the given view with this context
   * as the parent.
   */
  Context.prototype.push = function push (view) {
    return new Context(view, this);
  };

  /**
   * Returns the value of the given name in this context, traversing
   * up the context hierarchy if the value is absent in this context's view.
   */
  Context.prototype.lookup = function lookup (name) {
    var cache = this.cache;

    var value;
    if (cache.hasOwnProperty(name)) {
      value = cache[name];
    } else {
      var context = this, names, index, lookupHit = false;

      while (context) {
        if (name.indexOf('.') > 0) {
          value = context.view;
          names = name.split('.');
          index = 0;

          /**
           * Using the dot notion path in `name`, we descend through the
           * nested objects.
           *
           * To be certain that the lookup has been successful, we have to
           * check if the last object in the path actually has the property
           * we are looking for. We store the result in `lookupHit`.
           *
           * This is specially necessary for when the value has been set to
           * `undefined` and we want to avoid looking up parent contexts.
           **/
          while (value != null && index < names.length) {
            if (index === names.length - 1)
              lookupHit = hasProperty(value, names[index]);

            value = value[names[index++]];
          }
        } else {
          value = context.view[name];
          lookupHit = hasProperty(context.view, name);
        }

        if (lookupHit)
          break;

        context = context.parent;
      }

      cache[name] = value;
    }

    if (isFunction(value))
      value = value.call(this.view);

    return value;
  };

  /**
   * A Writer knows how to take a stream of tokens and render them to a
   * string, given a context. It also maintains a cache of templates to
   * avoid the need to parse the same template twice.
   */
  function Writer () {
    this.cache = {};
  }

  /**
   * Clears all cached templates in this writer.
   */
  Writer.prototype.clearCache = function clearCache () {
    this.cache = {};
  };

  /**
   * Parses and caches the given `template` and returns the array of tokens
   * that is generated from the parse.
   */
  Writer.prototype.parse = function parse (template, tags) {
    var cache = this.cache;
    var tokens = cache[template];

    if (tokens == null)
      tokens = cache[template] = parseTemplate(template, tags);

    return tokens;
  };

  /**
   * High-level method that is used to render the given `template` with
   * the given `view`.
   *
   * The optional `partials` argument may be an object that contains the
   * names and templates of partials that are used in the template. It may
   * also be a function that is used to load partial templates on the fly
   * that takes a single argument: the name of the partial.
   */
  Writer.prototype.render = function render (template, view, partials) {
    var tokens = this.parse(template);
    var context = (view instanceof Context) ? view : new Context(view);
    return this.renderTokens(tokens, context, partials, template);
  };

  /**
   * Low-level method that renders the given array of `tokens` using
   * the given `context` and `partials`.
   *
   * Note: The `originalTemplate` is only ever used to extract the portion
   * of the original template that was contained in a higher-order section.
   * If the template doesn't use higher-order sections, this argument may
   * be omitted.
   */
  Writer.prototype.renderTokens = function renderTokens (tokens, context, partials, originalTemplate) {
    var buffer = '';

    var token, symbol, value;
    for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) {
      value = undefined;
      token = tokens[i];
      symbol = token[0];

      if (symbol === '#') value = this.renderSection(token, context, partials, originalTemplate);
      else if (symbol === '^') value = this.renderInverted(token, context, partials, originalTemplate);
      else if (symbol === '>') value = this.renderPartial(token, context, partials, originalTemplate);
      else if (symbol === '&') value = this.unescapedValue(token, context);
      else if (symbol === 'name') value = this.escapedValue(token, context);
      else if (symbol === 'text') value = this.rawValue(token);

      if (value !== undefined)
        buffer += value;
    }

    return buffer;
  };

  Writer.prototype.renderSection = function renderSection (token, context, partials, originalTemplate) {
    var self = this;
    var buffer = '';
    var value = context.lookup(token[1]);

    // This function is used to render an arbitrary template
    // in the current context by higher-order sections.
    function subRender (template) {
      return self.render(template, context, partials);
    }

    if (!value) return;

    if (isArray(value)) {
      for (var j = 0, valueLength = value.length; j < valueLength; ++j) {
        buffer += this.renderTokens(token[4], context.push(value[j]), partials, originalTemplate);
      }
    } else if (typeof value === 'object' || typeof value === 'string' || typeof value === 'number') {
      buffer += this.renderTokens(token[4], context.push(value), partials, originalTemplate);
    } else if (isFunction(value)) {
      if (typeof originalTemplate !== 'string')
        throw new Error('Cannot use higher-order sections without the original template');

      // Extract the portion of the original template that the section contains.
      value = value.call(context.view, originalTemplate.slice(token[3], token[5]), subRender);

      if (value != null)
        buffer += value;
    } else {
      buffer += this.renderTokens(token[4], context, partials, originalTemplate);
    }
    return buffer;
  };

  Writer.prototype.renderInverted = function renderInverted (token, context, partials, originalTemplate) {
    var value = context.lookup(token[1]);

    // Use JavaScript's definition of falsy. Include empty arrays.
    // See https://github.com/janl/mustache.js/issues/186
    if (!value || (isArray(value) && value.length === 0))
      return this.renderTokens(token[4], context, partials, originalTemplate);
  };

  Writer.prototype.renderPartial = function renderPartial (token, context, partials) {
    if (!partials) return;

    var value = isFunction(partials) ? partials(token[1]) : partials[token[1]];
    if (value != null)
      return this.renderTokens(this.parse(value), context, partials, value);
  };

  Writer.prototype.unescapedValue = function unescapedValue (token, context) {
    var value = context.lookup(token[1]);
    if (value != null)
      return value;
  };

  Writer.prototype.escapedValue = function escapedValue (token, context) {
    var value = context.lookup(token[1]);
    if (value != null)
      return mustache.escape(value);
  };

  Writer.prototype.rawValue = function rawValue (token) {
    return token[1];
  };

  mustache.name = 'mustache.js';
  mustache.version = '2.3.0';
  mustache.tags = [ '{{', '}}' ];

  // All high-level mustache.* functions use this writer.
  var defaultWriter = new Writer();

  /**
   * Clears all cached templates in the default writer.
   */
  mustache.clearCache = function clearCache () {
    return defaultWriter.clearCache();
  };

  /**
   * Parses and caches the given template in the default writer and returns the
   * array of tokens it contains. Doing this ahead of time avoids the need to
   * parse templates on the fly as they are rendered.
   */
  mustache.parse = function parse (template, tags) {
    return defaultWriter.parse(template, tags);
  };

  /**
   * Renders the `template` with the given `view` and `partials` using the
   * default writer.
   */
  mustache.render = function render (template, view, partials) {
    if (typeof template !== 'string') {
      throw new TypeError('Invalid template! Template should be a "string" ' +
                          'but "' + typeStr(template) + '" was given as the first ' +
                          'argument for mustache#render(template, view, partials)');
    }

    return defaultWriter.render(template, view, partials);
  };

  // This is here for backwards compatibility with 0.4.x.,
  /*eslint-disable */ // eslint wants camel cased function name
  mustache.to_html = function to_html (template, view, partials, send) {
    /*eslint-enable*/

    var result = mustache.render(template, view, partials);

    if (isFunction(send)) {
      send(result);
    } else {
      return result;
    }
  };

  // Export the escaping function so that the user may override it.
  // See https://github.com/janl/mustache.js/issues/244
  mustache.escape = escapeHtml;

  // Export these mainly for testing, but also for advanced usage.
  mustache.Scanner = Scanner;
  mustache.Context = Context;
  mustache.Writer = Writer;

  return mustache;
}));


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.buildHistoryDonation = undefined;

var _donationFileList = __webpack_require__(1);

var buildHistoryDonation = function buildHistoryDonation() {
    var view = (0, _donationFileList.getHistoryDonations)();
    var donorsMarkup = "";
    var historyTable = document.getElementById("history-donation");

    for (var i = 0; i < view.length; i += 1) {
        var date = view[i][2].date;
        var quantity = view[i][2].quantity;

        donorsMarkup += "<tr>" + ("<td>" + (i + 1) + "</td>") + ("<td>" + date + "</td>") + ("<td>" + quantity + "</td>") + "</tr>";
    }
    historyTable.innerHTML += donorsMarkup;
};

exports.buildHistoryDonation = buildHistoryDonation;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.buildLastDonors = exports.buildUpdateDonorPage = exports.buildDonorPage = undefined;

var _mustache = __webpack_require__(3);

var _mustache2 = _interopRequireDefault(_mustache);

var _donorFileList = __webpack_require__(0);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var buildDonorPage = function buildDonorPage() {
  var view = (0, _donorFileList.getDonorFile)();
  var templateForDonorID = "File number: <span>{{donorID}}</span>";
  var templateForPersonalInfo = "<div>CNP: <span class='form'>{{cnp}}</span></div>" + "<div>Full Name <span class='form'>{{name}}</span></div>" + "<div>Date of Birth <span class='form'> {{dateOfBirth}}</span></div>" + "<div>Person Sex <span class='form'>{{sex}}</span></div>" + "<div>Address <span class='form'>{{address}}</span></div>" + "<div>Phone Number <span class='form'>{{phone}}</span></div>" + "<div>Email <span class='form'>{{email}}</span></div>";
  var templateForMedicalHistory = "<div>Health Condition <span class='form'>{{healthCondition}}</span></div>" + "<div>History of Diseases <span class='form'>{{historyDiseases}}</span></div>";
  var templateForBoodDetails = "<div>Blood Type: <span class='form'>{{bloodType}}</span></div>" + "<div>Blood Rh: <span class='form'>{{rh}}</span></div>" + "<div>Other Details: <span class='form'>{{otherDetails}}</span></div>";

  var personalInfo = _mustache2.default.render(templateForPersonalInfo, view[1]);
  var medicalHistory = _mustache2.default.render(templateForMedicalHistory, view[2]);
  var bloodDetails = _mustache2.default.render(templateForBoodDetails, view[3]);
  var donorID = _mustache2.default.render(templateForDonorID, view[0]);

  document.getElementById("donorID").innerHTML = donorID;
  document.getElementById("personalInfo").innerHTML = personalInfo;
  document.getElementById("medicalHistory").innerHTML = medicalHistory;
  document.getElementById("bloodDetails").innerHTML = bloodDetails;
  return [personalInfo, medicalHistory, bloodDetails, donorID];
};

var buildUpdateDonorPage = function buildUpdateDonorPage() {
  var view = (0, _donorFileList.getDonorFile)();
  var templateForDonorID = "File number: <span>{{donorID}}</span>";
  var templateForPersonalInfo = "<div>CNP" + "<input class='form' id='input-cnp' maxlength='11' value='{{cnp}}'>" + "</div>" + "<div>Full Name" + "<input class='form' type='text' id='input-name' value='{{name}}'>" + "</div>" + "<div>Date of Birth" + "<input class='form' type='date' value='{{dateOfBirth}}'>" + "</div>" + "<div>Person Sex" + "<input class='form' type='text' value='{{sex}}'>" + "</div>" + "<div>address" + "<input class='form' type='text' value='{{address}}'>" + "</div>" + "<div>Phone Number" + "<input class='form' type='tel' value='{{phone}}'>" + "</div>" + "<div>Email" + "<input class='form' type='email' value='{{email}}'>" + "</div>";
  var templateForMedicalHistory = "<textarea class='contact form' rows='7'>{{healthCondition}}</textarea>" + "<textarea class='contact form' rows='7'>{{historyDiseases}}</textarea>";
  var templateForBoodDetails = "<div>Blood Type" + "<input class='form' type='text' value='{{bloodType}}'>" + "</div>" + "<div>Rh" + "<input class='form' type='text' value='{{rh}}'>" + "</div>" + "<textarea class='contact form' rows='7'>{{otherDetails}}</textarea>";

  var donorID = _mustache2.default.render(templateForDonorID, view[0]);
  var personalInfo = _mustache2.default.render(templateForPersonalInfo, view[1]);
  var medicalHistory = _mustache2.default.render(templateForMedicalHistory, view[2]);
  var bloodDetails = _mustache2.default.render(templateForBoodDetails, view[3]);

  document.getElementById("donorID").innerHTML = donorID;
  document.getElementById("personalInfo").innerHTML = personalInfo;
  document.getElementById("medicalHistory").innerHTML = medicalHistory;
  document.getElementById("bloodDetails").innerHTML = bloodDetails;
  return [personalInfo, medicalHistory, bloodDetails, donorID];
};

var buildLastDonors = function buildLastDonors() {
  var view = (0, _donorFileList.getLastDonors)();
  var donorsMarkup = "";
  var historyTable = document.getElementById("last-donors-table");

  for (var i = 0; i < view.length; i += 1) {
    if (i < 10) {
      var bloodType = view[i][3].bloodType;
      var rh = view[i][3].rh;
      var name = view[i][1].name;

      donorsMarkup += "<tr>" + ("<td>" + (i + 1) + "</td>") + ("<td>" + name + "</td>") + ("<td>" + bloodType + "</td>") + ("<td>" + rh + "</td>") + "</tr>";
    }
  }
  historyTable.innerHTML += donorsMarkup;
};
exports.buildDonorPage = buildDonorPage;
exports.buildUpdateDonorPage = buildUpdateDonorPage;
exports.buildLastDonors = buildLastDonors;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.buildDonorsPerBloodType = undefined;

var _mustache = __webpack_require__(3);

var _mustache2 = _interopRequireDefault(_mustache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var buildDonorsPerBloodType = function buildDonorsPerBloodType() {
    var view = JSON.parse(localStorage.getItem("Lista"));
    var donorsMarkup = "";
    var periodTable = document.getElementById("donors-blood-type-table");

    for (var i = 0; i < view.length; i += 1) {
        var donorsName = view[i].donorsName;
        var donationsQuantity = view[i].donationsQuantity;
        var donationsDate = view[i].donationsDate;
        var getFromDay = new Date(view[i].periodStart).getDate();
        var getToDay = new Date(view[i].periodEnd).getDate();
        var getFromMonth = new Date(view[i].periodStart).getMonth();
        var getToMonth = new Date(view[i].periodEnd).getMonth();
        var getFromYear = new Date(view[i].periodStart).getFullYear();
        var getToYear = new Date(view[i].periodEnd).getFullYear();
        var templateForDate = "From <span>" + getFromDay + "/" + (getFromMonth + 1) + "/" + getFromYear + "</span>" + ("  To <span>" + getToDay + "/" + (getToMonth + 1) + "/" + getToYear + "</span>");
        var templateForTitle = "<span>{{bloodType}}</span>, BLOOD TYPE DONORS";
        donorsMarkup += "<tr>" + ("<td>" + (i + 1) + "</td>") + ("<td>" + donorsName + "</td>") + ("<td>" + donationsQuantity + "</td>") + ("<td>" + donationsDate + "</td>") + "</tr>";
        var bloodType = _mustache2.default.render(templateForTitle, view[i]);

        document.getElementById("pickDate").innerHTML = templateForDate;
        document.querySelector("h3").innerHTML = bloodType;
    }

    periodTable.innerHTML += donorsMarkup;
};

exports.buildDonorsPerBloodType = buildDonorsPerBloodType;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _utils = __webpack_require__(2);

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DonationFile = function DonorFile(data) {
    var donorID = JSON.parse(localStorage.getItem("DonorFile"));
    donorID = donorID[0];
    var donationID = (0, _utils2.default)();

    for (var i = 0; i < data.length; i += 1) {
        var date = data[0].value;
        var quantity = data[1].value;
        var gloves = data[2].value;
        var bandages = data[3].value;
        var syringes = data[4].value;
        var disinfectant = data[5].value;

        var donationInfo = {
            date: date,
            quantity: quantity
        };
        var donationMaterials = {
            gloves: gloves,
            bandages: bandages,
            syringes: syringes,
            disinfectant: disinfectant
        };

        return [donorID, donationID, donationInfo, donationMaterials];
    }
};

exports.default = DonationFile;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _utils = __webpack_require__(2);

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DonorFile = function DonorFile(data) {
    var donorID = (0, _utils2.default)();
    for (var i = 0; i < data.length; i += 1) {
        var cnp = data[0].value;
        var name = data[1].value;
        var dateOfBirth = data[2].value;
        var sex = data[3].value;
        var address = data[4].value;
        var phone = data[5].value;
        var email = data[6].value;
        var healthCondition = data[7].value;
        var historyDiseases = data[8].value;
        var bloodType = data[9].value;
        var rh = data[10].value;
        var otherDetails = data[11].value;

        var personalInfo = {
            cnp: cnp,
            name: name,
            dateOfBirth: dateOfBirth,
            sex: sex,
            address: address,
            phone: phone,
            email: email
        };
        var medicalHistory = {
            healthCondition: healthCondition,
            historyDiseases: historyDiseases
        };
        var bloodDetails = {
            bloodType: bloodType,
            rh: rh,
            otherDetails: otherDetails
        };

        var donorId = {
            donorID: donorID
        };

        return [donorId, personalInfo, medicalHistory, bloodDetails];
    }
};
exports.default = DonorFile;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _donorFileList = __webpack_require__(0);

var _buildDonorPage = __webpack_require__(5);

var _buildDonation = __webpack_require__(4);

var _donationFileList = __webpack_require__(1);

var _buildDonorsPerBloodType = __webpack_require__(6);

var _helpers = __webpack_require__(10);

var _bloodSupply = __webpack_require__(11);

(0, _bloodSupply.getBloodSupply)();
var search = document.querySelector(".search");
var pickDate = document.querySelector(".search-date");
var inputSearch = document.querySelector(".input-srch");

search.addEventListener("click", _donorFileList.searchDonorByCnp, false);
inputSearch.addEventListener("focus", _helpers.focusSearch, false);
inputSearch.addEventListener("blur", _helpers.blurSearch, false);

if (window.location.pathname === "/app-html/new-donor-page.html") {
    var saveFile = document.querySelector(".addNewDonor");
    var checkCnpBtn = document.getElementById("input-cnp");

    saveFile.addEventListener("click", _donorFileList.addDonor, false);
    checkCnpBtn.addEventListener("blur", _donorFileList.checkInputCnp, true);
    checkCnpBtn.addEventListener("focus", _donorFileList.clearInputCnp, true);
}
if (window.location.pathname === "/index.html") {
    (0, _buildDonorPage.buildLastDonors)();
    pickDate.addEventListener("click", _donorFileList.getDonorsByIntervalAndBloodType, false);
    inputSearch.addEventListener("focus", _helpers.focusSearch, false);
    inputSearch.addEventListener("blur", _helpers.blurSearch, false);
}

if (window.location.pathname === "/app-html/donor-profile.html") {
    (0, _buildDonorPage.buildDonorPage)();
    (0, _buildDonation.buildHistoryDonation)();
}

if (window.location.pathname === "/app-html/donor-update-page.html") {
    var updateDonor = document.querySelector(".update-donor");

    (0, _buildDonorPage.buildUpdateDonorPage)();
    updateDonor.addEventListener("click", _donorFileList.updateDonorFile, false);
}

if (window.location.pathname === "/app-html/add-new-donation-page.html") {
    var addDonationBtn = document.querySelector(".add-donation");

    addDonationBtn.addEventListener("click", _donationFileList.addDonation, false);
}

if (window.location.pathname === "/app-html/donors-per-bloodType.html") {
    (0, _buildDonorsPerBloodType.buildDonorsPerBloodType)();
}

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var search = document.querySelector(".search");
var inputSearch = document.querySelector(".input-srch");

var focusSearch = function focusSearch() {
    inputSearch.style.width = "340px";
    inputSearch.style.transition = "width 2s";
    search.style.left = "610px";
    search.style.transition = "left 2s";
};

var blurSearch = function blurSearch() {
    inputSearch.style.width = "180px";
    search.style.left = "450px";
};

exports.focusSearch = focusSearch;
exports.blurSearch = blurSearch;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var getBloodSupply = function getBloodSupply() {
  var donationsHistory = JSON.parse(localStorage.getItem("DonationsList"));
  console.log(donationsHistory);
  var arr = donationsHistory.map(function getBlood(donor) {
    console.log(donor);
  });
};

exports.getBloodSupply = getBloodSupply;

/***/ })
/******/ ]);